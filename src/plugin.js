import Toast from './toast.vue'

let currentToast
export default {
    install(Vue,options){
        Vue.prototype.$toast=function(msg,toastOptions){
            if(currentToast){
                currentToast.close()
            }
            currentToast=createToast({
                Vue,
                msg,
                propsData:toastOptions,
                onClose:()=>{
                    currentToast=null
                }
            })
        }
    }
}


function createToast({Vue,msg,propsData,onClose}){
    let Vm=Vue.extend(Toast)
    let toast=new Vm({propsData})
    toast.$slots.default=[msg]
    toast.$mount()
    toast.$on('close',onClose)
    document.body.append(toast.$el)
    return toast 
}