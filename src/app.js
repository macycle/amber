import Vue from 'vue'
import  Button from './button.vue';
import Icon from './icon.vue';
import ButtonGroup from './button-group.vue';
import Input from './input.vue';
import Row from './row.vue';
import Col from './col.vue';
import Content from './content.vue';
import Footer from './footer.vue';
import Header from './header.vue';
import Layout from './layout.vue';
import Sider from './sider.vue';
import Toast from './toast.vue';
import plugin from './plugin.js';
import Tabs from './tabs.vue';
import TabsBody from './tabs-body.vue';
import TabsHead from './tabs-head.vue';
import TabsItem from './tabs-item.vue';
import TabsPane from './tabs-pane.vue';
import Popover from './popover.vue';
import Collapse from './collapse.vue';
import CollapseItem from './collapse-item.vue'


Vue.component('am-button',Button);
Vue.component('am-icon',Icon);
Vue.component('am-group-button',ButtonGroup);
Vue.component('am-input',Input);
Vue.component('am-row',Row);
Vue.component('am-col',Col);
Vue.component('am-content',Content);
Vue.component('am-footer',Footer);
Vue.component('am-sider',Sider);
Vue.component('am-header',Header);
Vue.component('am-layout',Layout);
Vue.component('am-toast',Toast);
Vue.use(plugin);
Vue.component('am-tabs',Tabs);
Vue.component('am-tabs-item',TabsItem);
Vue.component('am-tabs-body',TabsBody);
Vue.component('am-tabs-head',TabsHead);
Vue.component('am-tabs-pane',TabsPane);
Vue.component('am-popover',Popover);
Vue.component('am-collapse',Collapse);
Vue.component('am-collapse-item',CollapseItem);



new Vue({
    el:'#app',
    data:{
        selectedTab:['1','2']
    },
    
    methods:{
        yyy(data){
            console.log('yyy'),
            console.log(data)
        }
    }
 
})