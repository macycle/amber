## Amber UI 一个移动端的UI库


[![Build Status](https://travis-ci.com/travis-ci/travis-web.svg?branch=master)](https://travis-ci.com/travis-ci/travis-web)

## 介绍


## 开始使用

#### 1.添加css样式
    使用本框架前，请在css中开启border-box

    ```
    *,*::before,*::after{box-sizing:border-box;}
    ```
    IE8及以上浏览器都支持此样式。


#### 2.安装amber
    ```
    npm i -S macycle1
    ```

#### 3.引入amber
    ```
    import {Button,ButtonGroup,Icon} from 'macycle1'
    import 'macycle1/dist/index.css'

    export default{
        name:'app',
        components:{
            'am-button':Button,
            'g-icon':Icon
        }
    }
    ```

#### 4.



## 文档

## 提问

## 变更记录

## 联系方式

## 贡献代码