## Amber UI 是什么

Amber UI 是我在深入研究Vue特性过程中尝试写的一套基于Framework7、Ant Design、Element UI的 UI 框架，改框架提供了一些常用的 UI 组件，适合 PC 端和移动端使用，该框架内的组件会一直更新下去，希望对你有所帮助。

目前已开发完成的组件有：按钮、输入框、grid布局、网格布局、Toast、Tabs、Popover、手风琴组件。

Radio、Select、Slider等组件将会陆续实现。

## 贡献
如果你还不清楚怎么在 GitHub 上提 Pull Request ，可以阅读下面这篇文章来学习：

[Pull Request](https://gitee.com/macycle/amber/pulls)


- 在项目根目录下运行了 npm install。

- 如果你修复了一个 bug 或者新增了一个功能，请确保写了相应的测试，这很重要。

- 确认所有的测试都是通过的 npm run test。 小贴士：开发过程中可以用 npm test -- --watch TestName 来运行指定的测试。

- 运行 npm test -- -u 来更新 jest snapshot 并且把这些更新也提交上来（如果有的话）。


## 联系方式
微信：18312516581

