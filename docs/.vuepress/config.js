module.exports = {
    base:'/amber/',
    title: 'Amber UI',
    description: '一套基于VUE开发的易用UI框架',
    themeConfig: {
      nav: [
        {text: '主页', link: '/'},
        {text: '欢迎 Star', link: 'https://gitee.com/macycle/amber'},
        {text: 'Github', link: 'https://gitee.com/macycle'}
    ],
      sidebar: [
        {
          title: '介绍',
          collapsable: false,
          children: [
              '/introduce/',
          ]
      },
        
        {
          title:'入门',
          collapsable: false,
          children:['/install/','/get-started/']
        },
        {
          title:'通用组件',
          collapsable: false,
          children:[
            '/components/button',
            '/components/grid',
            '/components/input',
            '/components/layout',
            '/components/popover',
            '/components/tabs',
            '/components/collapse',
            '/components/toast'
          ]
        }
      ]
    }
  }