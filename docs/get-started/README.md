---
title:快速上手
sidebarDepth:2
---

# 快速上手

## 添加CSS样式

::: warning
使用本框架前，请在 CSS 中开启 border-box
:::

```
*，*::before,*::after{ box-sizing: border-box }
```

## 引入Amber ui

```
import Vue from 'vue'
import {Button} from 'macycle1'
import 'macycle1/dist/index.css'
export default {
    name:'app',
    components:{
        'am-button':button
    }
}

```