---
home: true
heroImage: /amber.png
actionText: get start →
actionLink: /introduce/
features:
- title: 简明优先
  details: 极简化设计，注重内部功能实现
- title: Vue驱动
  details: 一套基于Vue的UI组件库
- title: 持续继承
  details: 自动化测试，保证每个组件的质量
footer: MIT Licensed | Copyright © 2020 Macycle Huang
---

