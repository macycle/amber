---
title:Popover 气泡卡片
sidebarDepth:2
---
# Popover 气泡卡片
点击/鼠标移入元素，弹出气泡式的卡片浮层。

## 何时使用
当目标元素有进一步的描述和相关操作时，可以收纳到卡片中，根据用户的操作行为进行展现。
## 代码演示

<ClientOnly>
<popover-demo1></popover-demo1>
</ClientOnly>
<br>

```js
    <am-popover position='left'>
        <template slot="content">
            left
        </template>
        <am-button>click me!</am-button>
    </am-popover>
```
<br>

## hover触发
<br>
<ClientOnly>
<popover-demo2></popover-demo2>
</ClientOnly>

<br>

```js
    <am-popover position='left' trigger="hover">
        <template slot="content">
            left
        </template>
        <am-button>hover me L!</am-button>
    </am-popover>
```

API
设置气泡卡片的属性。

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| position           | 气泡卡片显示方位 | String | top |
| trigger       | 气泡卡片触发方式      |   String | click |
| content | 卡片内容    |    String  |  - |
