---
title:Button 按钮
sidebarDepth:2
---

# Button 按钮
按钮用于开始一个即时操作。


## 何时使用
标记了一个（或封装一组）操作命令，响应用户点击行为，触发相应的业务逻辑。

## 代码演示

<br>
<ClientOnly>
<button-demos></button-demos>
</ClientOnly>

```js
<am-button>默认组件</am-button>
<am-button icon="settings">设置</am-button>
<am-button icon="thumbs-up">点赞</am-button>
<am-button icon="download" icon-position="right">下载</am-button>
<am-button :loading="true" icon="loading">登陆中</am-button>
<am-button disabled='true'>禁用按钮</am-button>
```

## 组合按钮

<br>
<ClientOnly>
<button-group-demos></button-group-demos>
</ClientOnly>

```js
<am-button-group>
    <am-button icon="left">上一页</am-button>
    <am-button icon="right" iconPosition="right">下一页</am-button>
</am-button-group>
```

## API
通过设置Button的属性来产生不同的按钮样式：

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| icon           | 设置按钮的图标组件 | String | - |
| icon-position       | 设置图标方向      |   String | - |
| loading  | 设置按钮载入状态     |    Boolean |  false |
| disabled  | 加载中     |    Boolean | false |