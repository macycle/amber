---
title:Layout 布局
sidebarDepth:2
---
# Layout 布局

协助进行页面级整体布局。

## 组件概述
- Layout：布局容器，其下可嵌套 Header Sider Content Footer 或 Layout 本身，可以放在任何父容器中。

- Header：顶部布局，自带默认样式，其下可嵌套任何元素，只能放在 Layout 中。

- Sider：侧边栏，自带默认样式及基本功能，其下可嵌套任何元素，只能放在 Layout 中。

- Content：内容部分，自带默认样式，其下可嵌套任何元素，只能放在 Layout 中。

- Footer：底部布局，自带默认样式，其下可嵌套任何元素，只能放在 Layout 中。


## 基本结构

<ClientOnly>
<layout-demo1></layout-demo1>
</ClientOnly>

<br>

```js 
<am-layout class="layout">
    <am-header class="header">header</am-header>
    <am-content class="content">content</am-content>
    <am-footer class="footer">footer</am-footer>
</am-layout>
```

<br>

<ClientOnly>
<layout-demo2></layout-demo2>
</ClientOnly>

<br>

```js
    <am-layout class="layout" >
        <am-header class="header">header</am-header>
        <am-layout has-sider>
            <am-sider class="sider">sider</am-sider>
            <am-content class="content">content</am-content>
        </am-layout>
        <am-footer class="footer">footer</am-footer>
    </am-layout>
```
<br>
<ClientOnly>
<layout-demo3></layout-demo3>
</ClientOnly>

<br>

```js
    <am-layout class="layout" has-sider>
        <am-sider class="sider">sider</am-sider>
        <am-layout>
            <am-header class="header">header</am-header>
            <am-content class="content">content</am-content>
            <am-footer class="footer">footer</am-footer>
        </am-layout>
    </am-layout>
```

<br>

## API
布局容器

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| className           | 容器 className | String | - |
| hasSider       | 表示子元素里有 Sider，一般不用指定。      |   boolean | - |
| style | 指定样式    |    CSSProperties |  - |
