---
title:Tabs 标签页
sidebarDepth:2
---
# Tabs 标签页
选项卡切换组件。

## 何时使用
提供平级的区域将大块内容进行收纳和展现，保持界面整洁。

## 代码演示
<br>

<ClientOnly>
<tabs-demos></tabs-demos>
</ClientOnly>

<br>
<br>

```js
    <am-tabs selected="tab1">
        <am-tabs-head>
            <am-tabs-item name='tab1'>tab1</am-tabs-item>
            <am-tabs-item name='tab2'>tab2</am-tabs-item>
            <am-tabs-item name='tab3'>tab3</am-tabs-item>
        </am-tabs-head>
        <am-tabs-body>
            <am-tabs-pane name='tab1'>内容1</am-tabs-pane>
            <am-tabs-pane name='tab2'>内容2</am-tabs-pane>
            <am-tabs-pane name='tab3'>内容3</am-tabs-pane>
        </am-tabs-body>
    </am-tabs>
```
<br>
<ClientOnly>
<tabs-demos2></tabs-demos2>
</ClientOnly>

<br>

```js
    <am-tabs selected="tab3">
        <am-tabs-head>
            <am-tabs-item name='tab1'>tab1</am-tabs-item>
            <am-tabs-item name='tab2' disabled>tab2</am-tabs-item>
            <am-tabs-item name='tab3'>tab3</am-tabs-item>
        </am-tabs-head>
        <am-tabs-body>
            <am-tabs-pane name='tab1'>内容1</am-tabs-pane>
            <am-tabs-pane name='tab2' >内容2</am-tabs-pane>
            <am-tabs-pane name='tab3'>内容3</am-tabs-pane>
        </am-tabs-body>
    </am-tabs>
```

## API

### Tabs
| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| selected           | 默认显示项 | String | - |

### Tabs-item
| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| name           | 每一项的名称 | String,Number | - |
| disabled       | 禁止选中 | Boolean | false |
