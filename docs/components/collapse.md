---
title:Collapse 折叠面板
sidebarDepth:2
---
# Collapse 折叠面板

可以折叠/展开的内容区域。

## 何时使用

对复杂区域进行分组和隐藏，保持页面的整洁。

手风琴 是一种特殊的折叠面板，只允许单个内容区域展开。


## 代码演示

<ClientOnly>
<collapse-demo1></collapse-demo1>
</ClientOnly>

<br>

```js
    <am-collapse :selected.sync="selectedTab" >
        <am-collapse-item title="panel 1" name='1'>内容1</am-collapse-item>
        <am-collapse-item title="panel 2" name='2'>内容2</am-collapse-item>
        <am-collapse-item title="panel 3" name='3'>内容3</am-collapse-item> 
    </am-collapse>
```

<br>


<ClientOnly>
<collapse-demo2></collapse-demo2>
</ClientOnly>
<br>

```js
    <am-collapse :selected.sync="selectedTab" single>
        <am-collapse-item title="panel 1" name='1'>内容1</am-collapse-item>
        <am-collapse-item title="panel 2" name='2'>内容2</am-collapse-item>
        <am-collapse-item title="panel 3" name='3'>内容3</am-collapse-item> 
    </am-collapse>
```

## API
Collapse

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| selected           | 默认选中项 | String | - |
| single      | 只能打开一项     |   Boolean | false |

Collapse-item
| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| title           | 折叠面板标题 | String | - |
| name           | 折叠面板名称 | String | - |