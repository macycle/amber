---
title:Grid 栅格
sidebarDepth:2
---
# Grid 栅格

24栅格系统。

## 设计理念
 Amber需要在设计区域内解决大量信息收纳的问题，因此在 12 栅格系统的基础上，我们将整个设计建议区域按照 24 等分的原则进行划分。

 ### 概述
 栅格化系统是基于行（row）和列（col）来定义信息区块的外部框架，以保证页面的每个区域能够稳健地排布起来。下面简单介绍一下它的工作原理：
 - 通过 row 在水平方向建立一组 column（简写 col）

 - 内容应当放置于 col 内，并且，只有 col 可以作为 row 的直接元素

- 栅格系统中的列是指 1 到 24 的值来表示其跨越的范围。例如，三个等宽的列可以使用 <am-col span="8" /> 来创建

- 如果一个 row 中的 col 总和超过 24，那么多余的 col 会作为一个整体另起一行排列

- 我们的栅格化系统基于 Flex 布局，允许子元素在父节点内的水平对齐方式 - 居左、居中、居右、等宽排列、分散排列。子元素与子元素之间，支持顶部对齐、垂直居中对齐、底部对齐的方式。

- 布局是基于 24 栅格来定义每一个『盒子』的宽度，但不拘泥于栅格。

## 代码演示
<ClientOnly>
<grid-demo1></grid-demo1>
</ClientOnly>

```js
<am-row class="demoRow">
        <am-col span="8"><div class="demoCol">33.33%</div></am-col>
        <am-col span="8"><div class="demoCol">33.33%</div></am-col>
        <am-col span="8"><div class="demoCol">33.33%</div></am-col>
    </am-row>
    <am-row class="demoRow">
        <am-col span="6"><div class="demoCol">25%</div></am-col>
        <am-col span="6"><div class="demoCol">25%</div></am-col>
        <am-col span="6"><div class="demoCol">25%</div></am-col>
        <am-col span="6"><div class="demoCol">25%</div></am-col>
    </am-row>
    <am-row class="demoRow">
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
        <am-col span="4"><div class="demoCol">16.67%</div></am-col>
    </am-row>
```

### 区间隔块

栅格常常需要和间隔进行配合，你可以使用 Row 的 gutter 属性，我推荐使用 (16+8n)px 作为栅格间隔(n 是自然数)。
<ClientOnly>
<grid-demo2></grid-demo2>
</ClientOnly>

```js
    <am-row class="demoRow" gutter="10">
        <am-col span="8"><div class="demoCol">8</div></am-col>
        <am-col span="8"><div class="demoCol">8</div></am-col>
        <am-col span="8"><div class="demoCol">8</div></am-col>
    </am-row>
    <am-row class="demoRow" gutter="10">
        <am-col span="6"><div class="demoCol">6</div></am-col>
        <am-col span="6"><div class="demoCol">6</div></am-col>
        <am-col span="6"><div class="demoCol">6</div></am-col>
        <am-col span="6"><div class="demoCol">6</div></am-col>      
    </am-row>
```

## 左右偏移
使用 offset 可以将列向右侧偏。例如，offset="8" 将元素向右侧偏移了 8 个列（column）的宽度

<ClientOnly>
<grid-demo3></grid-demo3>
</ClientOnly>

```js
    <am-row class="demoRow" gutter="10">
        <am-col span="8"><div class="demoCol">8</div></am-col>
        <am-col span="8" offset="8"><div class="demoCol">8</div></am-col>
    </am-row>
    <am-row class="demoRow" gutter="10">
        <am-col span="6" offset="6"><div class="demoCol">6</div></am-col>
        <am-col span="6" offset="6"><div class="demoCol">6</div></am-col>
    </am-row>
    <am-row class="demoRow" gutter="10">
        <am-col span="4"><div class="demoCol">4</div></am-col>
        <am-col span="4" offset="4"><div class="demoCol">4</div></am-col>
        <am-col span="4" offset="8"><div class="demoCol">4</div></am-col>
    </am-row>
    <am-row class="demoRow" gutter="10">
        <am-col span="2"><div class="demoCol">2</div></am-col>
        <am-col span="2" offset="2"><div class="demoCol">2</div></am-col>
        <am-col span="2"><div class="demoCol">2</div></am-col>
        <am-col span="2" offset="2"><div class="demoCol">2</div></am-col>
        <am-col span="2"><div class="demoCol">2</div></am-col>
        <am-col span="2" offset="2"><div class="demoCol">2</div></am-col>
        <am-col span="2"><div class="demoCol">2</div></am-col>
        <am-col span="2" offset="2"><div class="demoCol">2</div></am-col>   
    </am-row>
```

## API
通过设置Grid的属性来产生不同的Grid样式：

### Row

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| gutter           | 栅格间隔 | Number | 0 |

### Col
| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| span           | 栅格占位格数 | Number | - |
| offset           | 栅格左侧的间隔格数，间隔内不可以有栅格 | Number | 0 |