---
title:Input 输入框
sidebarDepth:2
---
# Input 输入框
通过鼠标或键盘输入内容，是最基础的表单域的包装。
## 常用例子

<br>

<ClientOnly>
<input-demos></input-demos>
</ClientOnly>

```js
<am-input value="中文"></am-input>
<am-input value="中文" disabled></am-input>
<am-input value="中文" error="错误"></am-input>
```

## 支持双向数据绑定

<br>

<input-model-demos></input-model-demos>

```js
<am-input v-model="value"></am-input>
<p>value:{{value}}</p>

data(){
    return{
        value:0
    }
}
```

## API
通过设置Input的属性来产生不同的输入样式：

| 属性           | 说明           | 类型  |  默认值 |
| -------------  |:-------------:| -----:|   -----:|
| value           | 输入框内容 | String | - |
| disabled      | 禁止输入      |   Boolean | false |
| error  | 错误提示     |    String |  - |
| success  | 成功提示     |    String | - |