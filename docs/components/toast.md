---
title:Toast 吐司
sidebarDepth:2
---

# Toast 吐司
类似吐司机一样弹出消息/通知

## 默认三个位置

<br>

<ClientOnly>
<toast-demo></toast-demo>
</ClientOnly>

```js
    <div>
        <am-button @click="showToastTop">Click me</am-button>
        <am-button @click="showToastTop">Top</am-button>
        <am-button @click="showToastMiddle">Center</am-button>
        <am-button @click="showToastBottom">Bottom</am-button>
    </div>
```
<br>

```js
methods:{
            showToastTop(){
                this.showToast('top')
            },
            showToastCenter(){
                this.showToast('center')
            },
            showToastBottom(){
                this.showToast('bottom')
            },
            showToast(position){
                this.$toast('this is content of toast',{
                    position:position,
                    autoClose:3,
                    closeButton:{
                        text:'Close',
                        callback(toast){
                            console.log('用户知道了')
                        }
                    }
                })
            }
        }
```

## 设置自动关闭

- 可设置Toats组件自动关闭，默认不能自动关闭。
- 可自行设置自动关闭延迟时间。

<br>
<ClientOnly>
<toast-auto-demo></toast-auto-demo>
</ClientOnly>

```js
    <div>
        <am-button @click="showToastTop">Click me</am-button>
    </div>
```
<br>


```js
methods:{
  showToastTop(){
    this.showToast('top')
  },
  showToast(position){
    this.$toast('This is content of toast',{
      position: position,
      autoClose: true,
      autoCloseDelay: 3,
      closeButton:{
        text: 'Close',
        callback(toast){
          console.log('用户知道了')
        }
      }
    })
  }
}
```

## 自定义Toast
- 可自定义设置 Toast 组件的中内容和关闭文本。
- 可自定义回调函数，进行 Toast 组件关闭后接下来的操作。

<br>
<ClientOnly>
<toast-custom-demo></toast-custom-demo>
</ClientOnly>

```js
<div>
    <am-button @click="showToastTop">Click me</am-button>
</div>
```
<br>

```js
methods:{
    showToastTop(){
      this.showToast('top')
    },
    showToast(position){
      this.$toast('自定义内容',{
        position: position,
        closeButton:{
          text: '请关闭',
          callback(toast){
            alert('用户知道了')
          }
        }
      })
    }
}
```
