const expect = chai.expect;
import Vue from 'vue'
import Tabs from '../src/tabs.vue'
import TabsBody from '../src/tabs-body.vue';
import TabsHead from '../src/tabs-head.vue';
import TabsItem from '../src/tabs-item.vue';
import TabsPane from '../src/tabs-pane.vue';
Vue.component('am-tabs',Tabs);
Vue.component('am-tabs-item',TabsItem);
Vue.component('am-tabs-body',TabsBody);
Vue.component('am-tabs-head',TabsHead);
Vue.component('am-tabs-pane',TabsPane);


Vue.config.productionTip = false
Vue.config.devtools = false

describe('Row', () => {
    it('存在.', () => {
        expect(Tabs).to.exist
    })      
    it('接受 selected 属性', (done) => {

        const div = document.createElement('div')
        document.body.appendChild(div)
        div.innerHTML = `
          <am-tabs selected="finance">
            <am-tabs-head>
              <am-tabs-item name="woman"> 美女 </am-tabs-item>
              <am-tabs-item name="finance"> 财经 </am-tabs-item>
              <am-tabs-item name="sports"> 体育 </am-tabs-item>
            </am-tabs-head>
            <am-tabs-body>
              <am-tabs-pane name="woman"> 美女相关资讯 </am-tabs-pane>
              <am-tabs-pane name="finance"> 财经相关资讯 </am-tabs-pane>
              <am-tabs-pane name="sports"> 体育相关资讯 </am-tabs-pane>
            </am-tabs-body>
          </am-tabs>
        `
        let vm = new Vue({
          el: div
        })
        vm.$nextTick(() => {
          let x = vm.$el.querySelector(`.tabs-item[data-name="finance"]`)
          expect(x.classList.contains('active')).to.be.true
          done()
        })
      })
    
      it('可以接受 direction prop', () => {
    
      })
       
})