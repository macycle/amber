const expect = chai.expect;
import Vue from 'vue'
import Input from '../src/input.vue'

Vue.config.productionTip = false
Vue.config.devtools = false

describe('Input', () => {
    it('存在.', () => {
        expect(Input).to.be.ok
    })

    describe('Props',()=>{
        const Constructor=Vue.extend(Input)
        let vm;
        afterEach(()=>{
            vm.$destroy()
        })


        it('接收value',()=>{
            vm=new Constructor({
                propsData:{
                    value:'123'
                }
            }).$mount()
            const inputElement=vm.$el.querySelector('input')
            expect(inputElement.value).to.eq('123')
        })
    
        it('接收disabled',()=>{
            vm=new Constructor({
                propsData:{
                    disabled:true
                }
            }).$mount()
            const inputElement=vm.$el.querySelector('input')
            expect(inputElement.disabled).to.eq(true)
        })
    
        it('接收readonly',()=>{
            vm=new Constructor({
                propsData:{
                    readonly:true
                }
            }).$mount()
            const inputElement=vm.$el.querySelector('input')
            expect(inputElement.readOnly).to.eq(true)
        })

        it('接收error',()=>{
            vm=new Constructor({
                propsData:{
                    error:'输入错误'
                }
            }).$mount()
            const useElement=vm.$el.querySelector('use')
            const errorMessage=vm.$el.querySelector('.errorMessage')
            expect(useElement.getAttribute('xlink:href')).to.eq('#am-error')
            expect(errorMessage.innerText).to.eq('输入错误')
        })

        it('接收success',()=>{
            vm=new Constructor({
                propsData:{
                    success:'输入成功'
                }
            }).$mount()
            const useElement=vm.$el.querySelector('use')
            const successMessage=vm.$el.querySelector('.successMessage')
            expect(useElement.getAttribute('xlink:href')).to.eq('#am-success')
            expect(successMessage.innerText).to.eq('输入成功')
        })
    })

    describe('事件',()=>{
        const Constructor=Vue.extend(Input)
        let vm;
        afterEach(()=>{
            vm.$destroy()
        })

        it('支持change/input/focus/blur事件',()=>{

            ['change','input','focus','blur']
            .forEach((eventName)=>{
                vm=new Constructor().$mount()
                const callback=sinon.fake();
                vm.$on(eventName,callback)
                let event=new Event(eventName);

                Object.defineProperty(
                    event,'target',{
                        value:{value:'hi'},enumerable:true
                    }
                )
                let inputElement=vm.$el.querySelector('input')
                inputElement.dispatchEvent(event)
                expect(callback).to.have.been.calledWith('hi')
            })  
        })

    })
    
})